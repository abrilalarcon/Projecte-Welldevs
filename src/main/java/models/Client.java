/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author eleme
 */
public class Client {
    private String CIF;
    private String Nom;
    private String Direcció;
    private String formaPagament;

    public Client(String CIF, String Nom, String Direcció, String formaPagament) {
        this.CIF = CIF;
        this.Nom = Nom;
        this.Direcció = Direcció;
        this.formaPagament = formaPagament;
    }

    public String getFormaPagament() {
        return formaPagament;
    }

    public String getCIF() {
        return CIF;
    }

    public String getNom() {
        return Nom;
    }

    public String getDirecció() {
        return Direcció;
    }
  
}
