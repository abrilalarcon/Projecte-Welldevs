/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

import java.util.Date;

/**
 *
 * @author eleme
 */
public class Comanda {
    private int codi;
    private String cifEmpresa;
    private int codiProducte;
    private Date dataComanda;

    public Comanda(int codi, String cifEmpresa, int codiProducte, Date dataComanda) {
        this.codi = codi;
        this.cifEmpresa = cifEmpresa;
        this.codiProducte = codiProducte;
        this.dataComanda = dataComanda;
    }
    public Date emmagatzemarData() {
        Date dataComanda = new Date();
        return dataComanda;
    }

    public int getCodi() {
        return codi;
    }

    public String getCifEmpresa() {
        return cifEmpresa;
    }

    public int getCodiProducte() {
        return codiProducte;
    }

    public Date getDataComanda() {
        return dataComanda;
    }
    
    
}
