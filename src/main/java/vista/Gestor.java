/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import exceptions.WellDevExceptions;
import java.sql.SQLException;
import models.Client;
import models.Producte;
import persistencia.InterfazDAO;

/**
 *
 * @author eleme
 */
public class Gestor {
    InterfazDAO pers = new InterfazDAO ();

    
    public void afegirClient(String cifClient, String nom, String direccio, String pagament) throws SQLException, WellDevExceptions {
        Client c = new Client(cifClient, nom, direccio, pagament);
        pers.insertarClient(c);
    }
    public void afegirProducte(String codi, String nom, String descripcio, double preu, int quantitat ) throws SQLException, WellDevExceptions {
        Producte p = new Producte(codi, nom, preu, descripcio, quantitat);
        pers.insertarProducte(p);
    }
    
}
