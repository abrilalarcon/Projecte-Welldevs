package exceptions;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author albay
 */
public class WellDevExceptions extends Exception{
    private static final int CODI_PRODUCTE = 0;
    private static final int PRODUCTE_CODI = 1;
    private static final int NOM_PRODUCTE_CODI = 2;
    private static final int NOM_PRODUCTE = 3;
    private static final int DESCRIPCIO_PRODUCTE = 4;  
    private static final int CIF_CLIENT = 5;
    private static final int CIF_DIGIT_CLIENT = 6;
    private static final int CIF_REGISTRAT = 7;
    private static final int DIRRECIO_EMPRESA = 8;
    private static final int FORMA_PAGAMENT = 9;
    private static final int NOM_EMPRESA = 10;
    private static final int ID_EMPRESA = 11;
   
    private final List<String> mensajes = Arrays.asList(
            "ERROR 001: Has de afegir un codi per aquest producte.",
            "ERROR 002: El codi a de tenir 5 lletres i numeros.",
            "ERROR 003: Aquest codi ja te un producte asignat. ",
            "ERROR 004: Has d'introduir un nom per el producte.",
            "ERROR 005: El producte necessita una descripció",
            "ERROR 006: Has d'intoduir un cif per el client.",
            "ERROR 007: El CIF ha de contenir 9 lletres i didgits.",
            "ERROR 008: Aquest CIF ja ha estat registrar.",
            "ERROR 009: Has de posar la direcció de l'empresa.",
            "ERROR 010: Selecciona la forma de pagament.",
            "ERROR 011: L'emprsa ha de tenir un nom",
            "ERROR 012: Es necesari introduir un cif per l'empresa"
    
            
    );
    
    
    private final int code;
    
    public WellDevExceptions (int code) {
        this.code = code;
    }
    
    @Override
    public String getMessage(){
        return mensajes.get(this.code);
    }
}
