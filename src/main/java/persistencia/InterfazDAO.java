package persistencia;
//prueba
import exceptions.WellDevExceptions;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JOptionPane;
import models.Client;
import models.Comanda;
import models.Producte;

/**
 *
 * @author albay
 */
public class InterfazDAO {
    
    private Connection conectar() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/erpventesbbdd";
        String user = "root";
        String pass = "1234";
        Connection c = DriverManager.getConnection(url, user, pass);
        return c;
    }
    
    private void desconectar(Connection c) throws SQLException {
        c.close();
    }
    public void insertarClient(Client client) throws SQLException, WellDevExceptions {
        if (existeixClient(client.getCIF())) {
           throw new WellDevExceptions (7);
        } 
        Connection c = conectar();
        PreparedStatement ps = c.prepareStatement("insert into clients values (?,?,?,?);");
        ps.setString(1, client.getNom());
        ps.setString(2,client.getCIF());
        ps.setString(3,client.getDirecció());
        ps.setString(4, client.getFormaPagament());
        ps.executeUpdate();
        ps.close();
        desconectar(c);
    }
    public boolean existeixClient(String cifEmpresa) throws SQLException {
        Connection c = conectar();
        Statement st = c.createStatement();
        String query = "select * from clients where cifClient = '" + cifEmpresa + "';";
        ResultSet rs = st.executeQuery(query);
        boolean existe = false;
        if (rs.next()) {
            existe = true;
        }
        rs.close();
        st.close();
        desconectar(c);
        return existe;
    }
    
    public void insertarProducte(Producte p) throws SQLException, WellDevExceptions {
        if (existeixProducte(p.getCodi())){
               throw new WellDevExceptions (2);  
        }
        Connection c = conectar();
        PreparedStatement ps = c.prepareStatement("insert into productes values (?,?,?,?,?);");
        ps.setString(1,p.getCodi());
        ps.setString(2, p.getNom());
        ps.setDouble(3, p.getPreu()); 
        ps.setString(4, p.getDescripcio());
        ps.setInt(5, p.getQuantitat());
        ps.executeUpdate();
        ps.close();
        desconectar (c);  
    }
    
    private boolean existeixProducte(String codi) throws SQLException {
        Connection c = conectar();
        Statement st = c.createStatement();
        String query = "select * from productes where codi = '" + codi + "';";
        ResultSet rs = st.executeQuery(query);
        boolean existe = false;
        if (rs.next()) {
            existe = true;
        }
        rs.close();
        st.close();
        desconectar(c);
        return existe;
    }
    
    public ArrayList<Client> allClients() throws SQLException {
        Connection c = conectar();
        ArrayList<Client> clients = new ArrayList<>();
        String query = "select * from clients;";
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery(query);
        while (rs.next()) {
            String nom = rs.getString("nomClient");
            String CIF = rs.getString("cifClient");
            String direccio = rs.getString("direccioClient");
            String formapagament = rs.getString("formaPagament");
            Client client = new Client(CIF, nom, direccio, formapagament);
            clients.add(client);
        }
        rs.close();
        st.close();
        desconectar(c);
        return clients;
    }
    public ArrayList<Producte> allProductes() throws SQLException {
        Connection c = conectar();
        ArrayList<Producte> productes = new ArrayList<>();
        String query = "select * from productes;";
        Statement st = c.createStatement();
        ResultSet rs = st.executeQuery(query);
        while (rs.next()) {
            String codi = rs.getString("codi");
            String nom = rs.getString("nom");
            String preu = rs.getString("preu");
            String descripcio = rs.getString("descripcio");
            String quantitat = rs.getString("quantitat");
            Double preudouble = Double.parseDouble(preu);
            int quantInt = Integer.parseInt(quantitat);
            Producte producte = new Producte(codi, nom ,preudouble, descripcio, quantInt);
            productes.add(producte);
        }
        rs.close();
        st.close();
        desconectar(c);
        return productes;
    }
    
    private boolean existeixComanda(int codi) throws SQLException {
        Connection c = conectar();
        Statement st = c.createStatement();
        String query = "select * from comanda where codi = '" + codi + ";";
        ResultSet rs = st.executeQuery(query);
        boolean existe = false;
        if (rs.next()) {
            existe = true;
        }
        rs.close();
        st.close();
        desconectar(c);
        return existe;
    }
    
    public void insertarComanda(Comanda co) throws SQLException, WellDevExceptions {
        if (existeixComanda(co.getCodi())) {
            throw new WellDevExceptions(3);
        }
        Connection c = conectar();
        PreparedStatement ps = c.prepareStatement("insert into productes values (?,?,?,?);");
        ps.setInt(1, co.getCodi());
        ps.setString(2, co.getCifEmpresa());
        ps.setInt(3, co.getCodiProducte());
        ps.setDate(4, (Date) co.getDataComanda());
        ps.executeUpdate();
        ps.close();
        desconectar(c);

    }
    
}
    